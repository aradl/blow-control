// Include the libraries we need
#include <OneWire.h>
#include <DallasTemperature.h>
#include "AvgStd.h"

// set pin definitions
#define ONE_WIRE_BUS 2
#define RELAY_CONTROL 3

// setup other parameters
#define SERIAL_PRINT false
#define ALARM_STD_DIV 1
#define ALARM_MIN_DEG_UP 2
#define ALARM_MIN_DEG_DOWN 2
#define INIT_COUNT 5
#define BLOW_WAIT_UP 2*60000 // two minutes
#define BLOW_WAIT_DOWN 5*60000 // five minutes
#define LOOP_WAIT 5*1000 // five seconds

// I don't recommend globals, but I am being lazy
// setup states
enum state_enum {BLOW_ON, BLOW_UP, BLOW_DOWN, BLOW_OFF};
uint8_t state = BLOW_OFF;
int itter = 0;

// setup anomally detection
AvgStd tempFs;

// Setup a oneWire instance to communicate with any OneWire devices
OneWire oneWire(ONE_WIRE_BUS);
// Pass our oneWire reference to Dallas Temperature. 
DallasTemperature sensors(&oneWire);

/*
 * The setup function. We only start the sensors here
 */
void setup(void)
{
  if(SERIAL_PRINT){
    // start serial port
    Serial.begin(9600);
    Serial.println("Hello!");
  }

  // Start up the library
  sensors.begin();  
  tempFs = AvgStd();
  tempFs.setRejectionSigma(7.0); //TODO figure this out

  // setup relay control  
  pinMode(RELAY_CONTROL, OUTPUT);
  
  if(SERIAL_PRINT){
    // setup csv header
    Serial.print("State");
    Serial.print(", Temp");
    Serial.print(", Alarm");
    Serial.println(", i");
  }
}

/*
 * Capture temperature values
 */
float get_tempF() {
  sensors.requestTemperatures();
  float tempC = sensors.getTempCByIndex(0);

  if(tempC != DEVICE_DISCONNECTED_C) 
  {
    return DallasTemperature::toFahrenheit(tempC);  
  } else {
    return tempC;
  }
}

/*
 * Determine if tempurature alarm is raised
 */
bool get_alarm(float inval) {  
  float mean = tempFs.getMean();
  float std  = tempFs.getStd();
  float tstd = ALARM_STD_DIV*std;

  // update value
  tempFs.checkAndAddReading(inval);
  
  // if values aren't valid yet
  if(std == -1){
    return false;
  }

  // detect drastic changes in temperature
  if(state == BLOW_OFF) {
    if((inval > (mean + tstd))
        && (tstd >=  ALARM_MIN_DEG_UP)){
          
      tempFs.reset();
      return true;
    }
    
  } else if(state == BLOW_ON) {
    if((inval < (mean - tstd))
        && (tstd >=  ALARM_MIN_DEG_DOWN)){
          
      tempFs.reset();
      return true;
    }
  }
  
  return false;
}

/*
 * Run state machine
 */
void state_machine_run(bool temp_alarm) 
{
  switch(state)
  {      
    case BLOW_OFF:
      if(temp_alarm == true){
        state = BLOW_UP;
      }
      break;
       
    case BLOW_UP:
      // turn on blower relay
      digitalWrite(RELAY_CONTROL, HIGH);

      // set next state, but wait some time
      state = BLOW_ON;      
      delay(BLOW_WAIT_UP);
      break;
 
    case BLOW_ON:
      if(temp_alarm == true){
        state = BLOW_DOWN;
      }
      break;
       
    case BLOW_DOWN:
      // turn on blower relay
      digitalWrite(RELAY_CONTROL, LOW);

      // set next state, but wait some time
      state = BLOW_OFF;      
      delay(BLOW_WAIT_DOWN);
      break;
  }
}

/*
 * Main function, get and show the temperature
 */
void loop(void)
{ 
  
  bool alarm  = false;
  float tempF = get_tempF();
  
  // Check if reading was successful
  if(tempF != DEVICE_DISCONNECTED_C) 
  {
    // run anomaly detector
    alarm = get_alarm(tempF);

    // get a few samples before starting
    if(itter > INIT_COUNT){
      state_machine_run(alarm);
    }
  }

  // sample wait time
  delay(LOOP_WAIT);
  itter++;
  if(SERIAL_PRINT){
    Serial.print(state);
    Serial.print(", ");
    Serial.print(tempF);
    Serial.print(", ");
    Serial.print(alarm, DEC);
    Serial.print(", ");
    Serial.println(itter);
  }
}
