# Blow Control

A simple Arduino based HVAC blower controller for my home.

## Requirements

Hardware
- An arduino board. (I am using a seeduino V3 <https://wiki.seeedstudio.com/Seeed_Arduino_Boards/>)
- A relay control board. (I made my own, but you could buy one here <https://www.sparkfun.com/products/13815>)

Software
- Temperature Controller Library <https://github.com/milesburton/Arduino-Temperature-Control-Library>
- Onewire <https://github.com/PaulStoffregen/OneWire>
- Helpful stats class<https://www.hackster.io/paskino/simple-data-statistics-temperature-c2d77c>
